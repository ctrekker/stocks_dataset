#[macro_use] extern crate log;
use serde::{Deserialize, Serialize};
use clap::{Arg, App, SubCommand, ArgMatches};
use rusqlite::{params, Connection, NO_PARAMS};
use std::error::Error;
use std::fs::File;
use simplelog::{CombinedLogger, TermLogger, LevelFilter};

#[derive(Serialize, Deserialize, Debug)]
#[allow(non_snake_case)]
struct StockSymbol {
    symbol: String,
    name: String,
    date: String,
    iexId: String,
    region: String,
    currency: String,
    isEnabled: bool
}
impl StockSymbol {
    fn empty(symbol: String) -> StockSymbol {
        StockSymbol {
            symbol,
            name: "".to_string(),
            date: "".to_string(),
            iexId: "".to_string(),
            region: "".to_string(),
            currency: "".to_string(),
            isEnabled: false
        }
    }
    fn get_clean_symbol(&self) -> String {
        self.symbol.as_str().replace("-", "_").replace(".", "__").replace("+", "___")
    }
}
#[derive(Serialize, Deserialize, Debug)]
#[allow(non_snake_case)]
struct HistoricalEntry {
    date:          String,
    open:          Option<f64>,
    high:          Option<f64>,
    low:           Option<f64>,
    close:         Option<f64>,
    volume:        Option<u32>,
    change:        Option<f64>,
    changePercent: Option<f64>
}
#[derive(Deserialize)]
struct Config {
    token: String
}

fn main() -> Result<(), Box<dyn Error>> {
    let matches = App::new("Stocks Dataset Utility Program")
        .version(env!("CARGO_PKG_VERSION"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .about(env!("CARGO_PKG_DESCRIPTION"))
        .subcommand(SubCommand::with_name("build")
            .about("populates database will all historical data")
            .arg(Arg::with_name("tables")
                .short("t")
                .help("only create tables")))
        .subcommand(SubCommand::with_name("update")
            .about("updates database with a single day")
            .arg(Arg::with_name("date")
                .short("d")
                .long("date")
                .takes_value(true)
                .help("provide custom date for update. YYYYMMDD format")))
//            .arg(Arg::with_name("interval")
//                .short("i")
//                .long("interval")
//                .takes_value(true)
//                .help("custom time interval back from current date")))
        .get_matches();

    CombinedLogger::init(
        vec![
            TermLogger::new(LevelFilter::Info, simplelog::Config::default()).unwrap(),
        ]
    ).unwrap();

    let config: Config = serde_yaml::from_reader(File::open("config.yaml")?)?;

    if let Some(matches) = matches.subcommand_matches("build") {
        build(config, matches)?;
    }
    if let Some(matches) = matches.subcommand_matches("update") {

    }

    Ok(())
}
fn init_db() -> Result<Connection, Box<dyn Error>>{
    let up = vec![
        r#"CREATE TABLE IF NOT EXISTS symbols (
            symbol       VARCHAR(8) PRIMARY KEY,
            name         TEXT NOT NULL,
            date         DATE,
            iex_id       VARCHAR(20),
            region       VARCHAR(40),
            currency     VARCHAR(25),
            is_enabled   BOOL
        )"#
    ];

    let conn = Connection::open("stocks.db")?;
    for cmd in up {
        if let Err(e) = conn.execute(cmd, params![]) {
            error!("{}", e);
        }
    }

    Ok(conn)
}
fn build(config: Config, matches: &ArgMatches) -> Result<(), Box<dyn Error>> {
    let mut conn: Connection = init_db()?;

    let mut symbols: Vec<StockSymbol> = vec![];
    {
        // Perform symbol indexing if not indexed
        let mut results = conn.prepare("SELECT symbol FROM symbols")?;
        let mut results = results.query(NO_PARAMS)?;
        while let Some(result) = results.next()? {
            let symbol: String = result.get(0)?;
            symbols.push(StockSymbol::empty(symbol));
        }
    }

    if symbols.is_empty() {
        info!("Symbols data missing. Downloading...");
        let res = reqwest::get(iex_url(String::from("/ref-data/symbols"), &config.token).as_str())?.text()?;
        symbols = serde_json::from_str(res.as_str())?;
        let tx = conn.transaction()?;
        for symbol in &mut symbols {
            if let Err(e) = tx.execute("INSERT INTO symbols (symbol, name, date, iex_id, region, currency, is_enabled) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7)",
                                params![&symbol.symbol, &symbol.name, &symbol.date, &symbol.iexId, &symbol.region, &symbol.currency, &symbol.isEnabled]) {
                error!("{}", e);
            }
        }
        tx.commit()?;
        info!("Finished downloading symbols data");
    }
    else {
        info!("Using cached symbols");
    }

    // Download historical data
    let mut i = 0;
    for symbol in &symbols {
        info!("{}/{} ({})", i, symbols.len(), symbol.symbol);

        conn.execute(format!(r#"CREATE TABLE IF NOT EXISTS `{}` (
            date           DATE PRIMARY KEY,
            open           DOUBLE NOT NULL,
            high           DOUBLE NOT NULL,
            low            DOUBLE NOT NULL,
            close          DOUBLE NOT NULL,
            volume         INTEGER NOT NULL
        )"#, symbol.get_clean_symbol()).as_str(), NO_PARAMS)?;

        // Don't perform queries if table flag is set
        if !matches.is_present("tables") {
            let res = reqwest::get(iex_url(String::from(format!("/stock/{}/chart/1m", symbol.symbol.as_str().replace("_", "-"))), &config.token).as_str())?.text()?;
            if res.contains("quota") {
                error!("Token usage has exceeded message quota. Exitting...");
                return Ok(());
            }
            let entries: Vec<HistoricalEntry> = serde_json::from_str(res.as_str())?;
            let tx  = conn.transaction()?;
            for entry in entries {
                if let Err(e) = tx.execute(format!("INSERT INTO `{}` (date, open, high, low, close, volume) VALUES (?1, ?2, ?3, ?4, ?5, ?6)", symbol.get_clean_symbol()).as_str(),
                                           params![&entry.date, &entry.open, &entry.high, &entry.low, &entry.close, &entry.volume]) {
                    error!("{}", e);
                }
            }
            tx.commit()?;
        }

        i += 1;
    }

    if let Err(e) = conn.close() {
        error!("{}", e.1);
    }
    Ok(())
}

fn iex_url(path: String, token: &String) -> String {
    return format!("https://cloud.iexapis.com/stable{}?token={}", path, token);
}
